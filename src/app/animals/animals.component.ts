import { Component, OnInit } from '@angular/core';
import { Animal } from '../animal';
import { ANIMALS } from '../mock-animals';
 
@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

  public static numberOfInstance = 0;

  animals = ANIMALS;

  animal: Animal = {
     id: 1,
     name: 'Cat',
     imgUrl: ''
   }

  selectedAnimal: Animal;
  onSelect(animal:Animal): void{
    this.selectedAnimal = animal;
  }
  constructor() { 


  }

  ngOnInit(): void {
  }

}
